#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Beverage {

string description = "Unknown Description";

public:
	void getDescription() {
		cout << description << endl;
	}

	void setDescription(const string& customDesc) {
		description = customDesc;
	}
	
	virtual int cost() = 0;
	virtual ~Beverage() {};
};

class MasalaTea: public Beverage {
public:
	MasalaTea() {
		setDescription("Masala tea is awesome with 10 spices");
	}

	int cost() {
		return 7;
	}

};

class ChocolateTea: public Beverage {
public:
	ChocolateTea() {
		setDescription("Chocolate tea is yummy with chocolate");
	}

	int cost() {
		return 5;
	}
};

class GingerTea: public Beverage {
public:
	GingerTea() {
		setDescription("Ginger tea is good for digestion");
	}

	int cost() {
		return 2;
	}
};


// Driver code
// PROBLEM : class explosion.
// cannot able to customize the product - no sugar tea, Normal tea, tea without milk etc...
int main() {
	// Beverage* mtea = new MasalaTea();
	unique_ptr<Beverage> mtea(new MasalaTea());

	// Beverage* gtea = new GingerTea();
	unique_ptr<Beverage> gtea(new GingerTea());
	
	
	cout << "Cost of masala tea is " << mtea->cost() << endl;
	cout << "Cost of ginger is " << gtea->cost() << endl;
	
	// delete mtea;
	// delete gtea
	cin.get();
}
