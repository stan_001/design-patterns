#include <iostream>
#include <memory>
using namespace std;

class Beverage {
	int _cost = 0;
	string description = "Unknown Description" ;
	bool masala, ginger, chocolate, milk, teaPowder, water, sugar;

public:

	void getDescription() {
		cout << description << endl;
	}

	void setDescription(const string& customDesc) {
		description = customDesc;
	}

	bool hasMasala() { return masala; }
	bool hasGinger() { return ginger; }
	bool hasChocolate() { return chocolate; }
	bool hasMilk() { return milk; }
	bool hasTeaPowder() { return teaPowder; }
	bool hasWater() { return water; }
	bool hasSugar() { return sugar; }

	// this can be avaoided and we can pass in constructor too.
	void setMasala() { masala = true; }
	void setGinger() { ginger = true; }
	void setChocolate() { chocolate = true; }
	void setMilk() { milk = true; }
	void setTeaPowder() { teaPowder = true; }
	void setWater() { water = true; }
	void setSugar() { sugar = true; }

	virtual int cost() { // why virtual - open closed principle - later if someone feels to update recipe for masala tea then :p masala + ginger
		if (hasMasala()) _cost+=3;
		if (hasGinger()) _cost += 1;
		if (hasChocolate()) _cost+=2;
		if (hasMilk()) _cost+=1;
		if (hasTeaPowder()) _cost+=1;
		if (hasWater()) _cost+=1;
		if (hasSugar()) _cost+=1;
		return _cost;
	}
	
	virtual ~Beverage() {};

};

class MasalaTea: public Beverage {
public:
	MasalaTea() {
		setDescription("Masala tea is awesome with 10 spices");
	}
};

class ChocolateTea: public Beverage {
public:
	ChocolateTea() {
		setDescription("Chocolate tea is yummy with chocolate");
	}
};

class GingerTea: public Beverage {
public:
	GingerTea() {
		setDescription("Ginger tea is good for digestion");
	}
};

// Problem
// double chocolate , strong tea - double teapowder , more sugar
// returning boolean invokes another conditional statement again.
// Change of source code for strong items / new items
// interface segragation principle - subclass should not be forced to depend on methods that it does not use. Introduce Coffee() most methods will have acces to invoke tea :(
// solution: have more smaller interfaces than once large interface
int main() {
	unique_ptr<Beverage> masalaTea(new MasalaTea());
	masalaTea->setMilk();
	masalaTea->setTeaPowder();
	masalaTea->setSugar();
	masalaTea->setMasala();

	cout << "Masala tea cost " << masalaTea->cost() << endl;

	unique_ptr<Beverage> gingerTea(new GingerTea());
	gingerTea->setMilk();
	gingerTea->setTeaPowder();
	gingerTea->setSugar();
	gingerTea->setGinger();

	cout << "Ginger tea cost " << gingerTea->cost() << endl;
	cin.get();
}