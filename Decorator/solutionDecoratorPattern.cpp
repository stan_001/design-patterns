// in case of maintaining wrapper order - decorator is not good pattern to go with.
// wrapping cost - inner most is base condition 
// we can add any Beverage - coffee , juice etc
// we need not worry about altering recipe for a primary item (tea, coffee, juice)
// we can add triple / n time the addons.
// We need not override all methods.
// no need for if condition issues.

#include <iostream>
#include <memory>

using namespace std;

class Beverage {
public:
	string description = "";

	virtual string getDescription() {
		return description;
	}

	void setDescription(const string& customDesc) {
		description += customDesc;
	}

	virtual int cost() = 0;

	virtual ~Beverage() {};

};

// -------------------------------------------------------------------------
/*----------- Base items of Beverage --------------*/

class TeaPowder: public Beverage {
public:
	TeaPowder() {
		setDescription("Tea Powder");
	}

	int cost() {
		return 1;
	}
};

// new requirement
class CoffeePowder: public Beverage {
public:
	CoffeePowder() {
		setDescription("Coffee Powder");
	}

	int cost() {
		return 2;
	}
};


// ----------------------------------------------------------------------------
class AddOnDecorator: public Beverage {

protected:
	shared_ptr<Beverage> m_Beverage;

public:

	AddOnDecorator(shared_ptr<Beverage> Beverage) : m_Beverage(Beverage) {} 

	virtual string getDescription() = 0;
};

// -----------------------------------------------------------------------------
class Chocolate: public AddOnDecorator {
public:
	
	Chocolate(shared_ptr<Beverage> Beverage) : AddOnDecorator(Beverage) {
	}

	string getDescription() override {
		return m_Beverage->getDescription() + " , Chocolate" ;
	}

	int cost() override {
		return m_Beverage->cost() + 4;
	}
};

class Ginger: public AddOnDecorator {

public:
	Ginger(shared_ptr<Beverage> Beverage) : AddOnDecorator(Beverage) {
	}

	string getDescription() override {
		return m_Beverage->getDescription() + " , Ginger" ;
	}
	int cost() override {
		return m_Beverage->cost() + 2;
	}
};
// ----------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------
// driver code
int main() {

	shared_ptr<Beverage> tea(new TeaPowder());

	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	tea = shared_ptr<Beverage> (new Chocolate(tea));

	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	tea = shared_ptr<Beverage> (new Ginger(tea));

	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	cin.get();
}
// ------------------------------------------------------------------------------------------

// Below is implementation of decorator pattern using raw pointers.

/* ------------- Native decorator start -------------- */
// Abstract Class
class NativeAddOnDecorator: public Beverage {

protected:
	Beverage* m_Beverage;

public:

	NativeAddOnDecorator(Beverage* Beverage) : m_Beverage(Beverage) {} 

	virtual string getDescription() = 0;
};

class NativeChocolate: public NativeAddOnDecorator {
public:
	
	NativeChocolate(Beverage* Beverage) : NativeAddOnDecorator(Beverage) {
	}

	string getDescription() override {
		return m_Beverage->getDescription() + " , Chocolate" ;
	}

	int cost() override {
		return m_Beverage->cost() + 4;
	}
};

class NativeGinger: public NativeAddOnDecorator {

public:
	NativeGinger(Beverage* Beverage) : NativeAddOnDecorator(Beverage) {
	}

	string getDescription() override {
		return m_Beverage->getDescription() + " , Ginger" ;
	}
	int cost() override {
		return m_Beverage->cost() + 2;
	}
};

// driver code
int nativeMain() {
	Beverage* tea = new TeaPowder();
	
	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;
	
	tea = new NativeChocolate(tea);
	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	tea = new NativeGinger(tea);
	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	tea = new NativeGinger(tea);
	cout << "COMPONENTS : " << tea->getDescription() << ", COST: " << tea->cost() << endl;

	delete tea;
	return 0;
}
/* ---------------------------- Native decotator end -----*/






// class Milk: public AddOnDecorator {

// 	Beverage Beverage;

// public:
// 	string getDescription() {
// 		return this.Beverage + " ,Milk" << endl;
// 	}
// 	int cost() {
// 		return this.Beverage.cost + 2;
// 	}
// };

// class Sugar: public AddOnDecorator {
// Beverage Beverage;
// public:
// 	void getDescription() {
// 		return this.Beverage + " ,Sugar" << endl;
// 	}
// 	int cost() {
// 		return this.Beverage.cost + 1;
// 	}
// };

// class Masala: public AddOnDecorator {
// Beverage Beverage;
// public:
// 	void getDescription() {
// 		return this.Beverage + " ,Masala" << endl;
// 	}
// 	int cost() {
// 		return this.Beverage.cost + 3;
// 	}
// };






















