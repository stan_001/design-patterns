#ifdef BASIC_EXAMPLE

#include <iostream>
#include <memory>
#include <optional>

using namespace std;

// GOF builder design pattern is creation pattern - alwasy use cpp17 to compile this code.

enum class EngineType
{
    CC_5000,
    CC_4000
};

enum class TyreType
{
    RADIAL,
    TUBE
};

struct Engine
{

    Engine(EngineType et) : m_et(et) {}

    EngineType m_et;
};

struct Tyre
{
    Tyre(TyreType tt) : m_tt(tt)
    {
    }

    TyreType m_tt;

    int getMtt()
    {
        return (int)m_tt;
    }
};

struct AirBag
{
    int m_noa = 0;

    AirBag(int numberOfAirBags) : m_noa(numberOfAirBags) {}
};

class Car
{ // This way of constructing object create mutability - it is sick!
public:
    Car(Engine *e, Tyre *t) : m_e(e), m_t(t)
    { // permutation combination problem.
        cout << "2 argument constructor is invoked " << endl;
    }

    Car(Engine *e, Tyre *t, AirBag *ab) : m_e(e), m_t(t), m_ab(ab)
    {
        cout << "3 argument constructor is invoked " << endl;
    }

    Car(Engine *e, optional<Tyre *> t = std::nullopt) : m_e(e), m_t(t)
    {
        cout << "optional constructor triggered" << endl;
    }

    Engine *m_e;
    optional<Tyre *> m_t;
    AirBag *m_ab;
};

int main()
{
    auto e = new Engine(EngineType::CC_4000);
    auto t = new Tyre(TyreType::TUBE);
    auto a = new AirBag(0);

    auto car = make_shared<Car>(e);
    cout << "Engine Type " << (int)car->m_e->m_et << endl;

    if (car->m_t != nullopt)
    {                                                                 // nullptr check will not work here, for optional always use nullopt
        cout << "Tyre Type" << (int)(car->m_t).value()->m_tt << endl; // use value_or in case of default values.
    }
    else
    {
        try
        {
            cout << (int)(car->m_t).value()->m_tt << endl;
        }
        catch (const std::exception &exc)
        {
            std::cerr << exc.what();
        }
    }

    return 0;
}
#endif

// Example of creating object using builder pattern conditionally i.e if some parameters are presnet then create this object and vice versa

#include <iostream>
#include <string>

// Product class
class House {
public:
    void setFoundation(const std::string& foundation) {
        foundation_ = foundation;
    }

    void setWalls(const std::string& walls) {
        walls_ = walls;
    }

    void setRoof(const std::string& roof) {
        roof_ = roof;
    }

    void showDetails() const {
        std::cout << "House with " << foundation_ << " foundation, "
                  << walls_ << " walls, and " << roof_ << " roof." << std::endl;
    }

private:
    std::string foundation_;
    std::string walls_;
    std::string roof_;
};

// Builder interface
class HouseBuilder {
public:
    virtual void buildFoundation() = 0;
    virtual void buildWalls() = 0;
    virtual void buildRoof() = 0;
    virtual House getResult() const = 0;
};

// Concrete builder implementing HouseBuilder
class StandardHouseBuilder : public HouseBuilder {
public:
    void buildFoundation() override {
        house_.setFoundation("Standard");
    }

    void buildWalls() override {
        house_.setWalls("Brick");
    }

    void buildRoof() override {
        house_.setRoof("Tile");
    }

    House getResult() const override {
        return house_;
    }

private:
    House house_;
};

// Director class - orchestrator class used to mandate params.
class HouseDirector {
public:
    House constructHouse(HouseBuilder& builder) {
        builder.buildFoundation();
        builder.buildWalls();
        builder.buildRoof();
        return builder.getResult();
    }
};

int main() {
    HouseDirector director;
    
    StandardHouseBuilder standardBuilder;
    House standardHouse = director.constructHouse(standardBuilder);
    standardHouse.showDetails();

    return 0;
}