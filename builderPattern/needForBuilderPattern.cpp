#include<iostream>
#include<string>
#include <memory>
/**
 * @brief Need for Builder pattern
 * - Immutability
 * - (Monsterous constructor) Class construction has many parameters then builder is go to option
 * - Class if there are params which are optional then builder is go to option
 * - Complex object construction which takes laborous work to construct.
 */

using namespace std;

class Engine  {};
class Tyre  {};
class SunRoof  {
    public: 
        SunRoof(string sunRoof): m_sunRoof(sunRoof) {
        }
        string getName() {
            return m_sunRoof;
        }
    private:
        string m_sunRoof;
};
class AirBag {};

class Car {
    protected:
        class CarBuilder;
    
    public:
        static CarBuilder builder() {
            return CarBuilder(); // createing builder in stack
        }

        shared_ptr<Engine> getEngine() { return m_engine; }
        shared_ptr<Tyre> getTyre() { return m_tyre; }
        shared_ptr<SunRoof> getSunRoof() { return m_sunRoof; }

    protected:
        shared_ptr<Engine> m_engine;
        shared_ptr<Tyre> m_tyre;
        shared_ptr<SunRoof> m_sunRoof;
        shared_ptr<AirBag> m_airBag;
        
        class CarBuilder {
            public:
            CarBuilder() {
                cout << "creating" << endl;
            }
            ~CarBuilder() {
                cout << "Destuctor called for car builder" << endl;
            }
            private:
                shared_ptr<Engine> m_engine;
                shared_ptr<Tyre> m_tyre;
                shared_ptr<SunRoof> m_sunRoof;
                shared_ptr<AirBag> m_airBag;

            public:
                CarBuilder& withEngine(shared_ptr<Engine> engine) {
                    cout << "Adding Engine" << endl;
                    this->m_engine = engine;
                    return *this;
                }

                CarBuilder& withTyre(shared_ptr<Tyre> tyre) {
                    cout << "Adding Tyre" << endl;
                    this->m_tyre = tyre;
                    return *this;
                }

                CarBuilder& withSunRoof(shared_ptr<SunRoof> sunroof) {
                    cout << "Adding SunRoof" << endl;
                    this->m_sunRoof = sunroof;
                    return *this;
                }

                CarBuilder& withAirBag(shared_ptr<AirBag> airBag) {
                    cout << "Adding AirBag" << endl;
                    this->m_airBag = airBag;
                    return *this;
                }

                shared_ptr<Car> build() {
                    if (m_engine == nullptr || m_tyre == nullptr) {
                        throw invalid_argument("Invalid arguments provided");
                        return nullptr;
                    }

                    auto carInstance = make_shared<Car>();
                    carInstance->m_airBag = move(m_airBag);
                    carInstance->m_engine = move(m_engine);
                    carInstance->m_sunRoof = move(m_sunRoof);
                    carInstance->m_tyre = move(m_tyre);
                    return carInstance;                    
                }
        };
};

int main() {
    auto audi = Car::builder().withTyre(make_shared<Tyre>()).withEngine(make_shared<Engine>()).build();
    auto skoda = Car::builder().withTyre(make_shared<Tyre>()).withEngine(make_shared<Engine>()).withSunRoof(make_shared<SunRoof>("full")).withSunRoof(make_shared<SunRoof>("half")).build();
    auto bmw = Car::builder().withTyre(make_shared<Tyre>()).withEngine(make_shared<Engine>()).build();

    if (audi->getSunRoof() == nullptr) { cout << " test " ;}
    cout << skoda->getSunRoof()->getName();
}