## Factory Pattern
Rather asking client handle logic of creating specific class , let factory component take care of object creation.

### Situation
* Class construction is complicated, there are multiple dependencies to instantiate an object.
* Client code need to be updated based on the new dependencies. For example, Object duck is needed by client and client need to pass duck's arguments to create specific type of duck. There are 2 problems here 
    * Client need to provide instance of Strategies mentioned - ISwinBehavoir and IFlybehavoir, client should be aware of how these stragegy classes are created.
    * In future if any of arguments passed got updated then client need to update instantion code.
    * [IMP] there could be any variation of duck classes needed for client RubberDuck, WildDuck etc... so client need to maintain all these dependent items creation and deletion.
```cpp
   IDuck* duck = new Duck("Rubber", ISwimBehavior* isb, IFlyBehavior* ifb);
```

### Solution
* Create a Factory class which takes care of all these dependent works.
* let client pass only the type of object needed to factory.
* getDuckInstance() method from factory will return the object instance.
* getDuckInstance() will delegate actual object creation to respective classes.

### Important things when implementing factory
* getObject(string ObjectName), method need to be static and clients will access directly via "InterfaceVehicle::getInstance()"
