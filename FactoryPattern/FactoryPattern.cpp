#include <iostream>
#include <memory>

using namespace std;

class IVehicle {
    public:
    static unique_ptr<IVehicle> getVehicle(string vehicleType);
    virtual void makeSound() = 0;
};

class Bike: public IVehicle {
    public:
    void makeSound() {
        cout << "Bike Sound" << endl;
    }
};

class Car: public IVehicle {
    public:
    void makeSound() {
        cout << "Car Sound" << endl;
    }
};

unique_ptr<IVehicle> IVehicle::getVehicle(string vehicleType) {
    // all complex creation logic goes here, in case of any dependecy to
    // child classes then those will be delegated to construtor of objects.
    if(vehicleType == "car") {
        return make_unique<Car>();
    }
    if(vehicleType == "bike") {
        return make_unique<Bike>();
    }
    return nullptr;
}

int main() {
    // client code
    string choice;
    cout << "Enter type of vehicle you need" << endl;
    cin >> choice;

    auto vehicleObj = IVehicle::getVehicle(choice);
    vehicleObj->makeSound();
    return 0;
}