#include <iostream>
#include <memory>

using namespace std;

class IVehicle
{
public:
    virtual void makeSound() = 0;
};

class Bike : public IVehicle
{
public:
    void makeSound()
    {
        cout << "Bike Sound" << endl;
    }
};

class Car : public IVehicle
{
public:
    void makeSound()
    {
        cout << "Car Sound" << endl;
    }
};

int main()
{
    // client code
    string choice;
    cout << "Enter type of vehicle you need" << endl;
    cin >> choice;

    if (choice.compare("bike") == 0)
    {
        auto b = make_unique<Bike>();
        b->makeSound();
    }

    if (choice.compare("car") == 0)
    {
        auto c = make_unique<Car>();
        c->makeSound();
    }

    return 0;
}
