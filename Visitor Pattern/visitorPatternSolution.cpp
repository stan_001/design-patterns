#include <iostream>
#include <vector>
using namespace std;


// mainly used to solve problem of temproary algorithms or interactions.
// need to add functionality into core logic without asking those components to make changes.
// 2 objects interactions in matrix.
// |---------------------------------------------------|
// |        HotelOffer | GasOffer | AirportLoungeOffer |
// |---------------------------------------------------|
// |Gold  |     30%    |     28%  |        21%         |
// |---------------------------------------------------|
// |Silver|     25%    |     18%  |        13%         |
// |---------------------------------------------------|
// |Bronze|     11%    |      8%  |         5%         |
// |---------------------------------------------------|

class IOfferVisitor;

class ICards {
    public:
    virtual string getName() = 0;
    virtual void accept(IOfferVisitor* visitor) = 0;
};

class GoldCard: public ICards {
    public:
    string getName() {
        return "Gold";
    }

    void accept(IOfferVisitor* visitor) {
        visitor->visitGoldCard(this);
    }
};
class SilverCard: public ICards {
    public:
    string getName() {
        return "Silver";
    }

    void accept(IOfferVisitor* visitor) {
        visitor->visitSilverCard(this);
    }
};
class BronzeCard: public ICards {
    public:
    string getName() {
        return "Bronze";
    }

    void accept(IOfferVisitor* visitor) {
        visitor->visitBronzeCard(this);
    }
};

class IOfferVisitor{
    public:
        virtual void visitGoldCard(GoldCard* gc) = 0;
        virtual void visitSilverCard(SilverCard* sc) = 0;
        virtual void visitBronzeCard(BronzeCard* bc) = 0;
};

class HotelOffer: public IOfferVisitor{
    public:
    void visitGoldCard(GoldCard* gc) {
        cout << "Card::" << gc->getName() << ",HotelOffer::" << endl;
    }
    void visitSilverCard(SilverCard* sc) {
        cout << "Card::" << sc->getName() << ",HotelOffer::" << endl;
    }
    void visitBronzeCard(BronzeCard* bc) {
        cout << "Card::" << bc->getName() << ",HotelOffer::" << endl;
    }
};

class GasOffer: public IOfferVisitor{
    void visitGoldCard(GoldCard* gc) {
        cout << "Card::" << gc->getName() << ",GasOffer::" << endl;
    }
    void visitSilverCard(SilverCard* sc) {
        cout << "Card::" << sc->getName() << ",GasOffer::" << endl;
    }
    void visitBronzeCard(BronzeCard* bc) {
        cout << "Card::" << bc->getName() << ",GasOffer::" << endl;
    }
};

class AirportLoungeOffer: public IOfferVisitor{
    void visitGoldCard(GoldCard* gc) {
        cout << "Card::" << gc->getName() << ",AirportLoungeOffer::" << endl;
    }
    void visitSilverCard(SilverCard* sc) {
        cout << "Card::" << sc->getName() << ",AirportLoungeOffer::" << endl;
    }
    void visitBronzeCard(BronzeCard* bc) {
        cout << "Card::" << bc->getName() << ",AirportLoungeOffer::" << endl;
    }
};

int main () {
    IOfferVisitor* hotelOffer = new HotelOffer;
    vector<ICards*> vec = {
        new GoldCard,
        new SilverCard,
        new BronzeCard
    };
    for (auto card: vec) {
        card->accept(hotelOffer);
    }
}