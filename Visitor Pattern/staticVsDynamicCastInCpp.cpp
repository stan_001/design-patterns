#include <iostream>

using namespace std;


class Base {
    virtual void f() {}
};

class Derived1: public Base {
    int value;
    public:
    Derived1() {
        value = 10;
    }
    void print() {
        cout << "Derived1 class value is " << value;
    }
};

class Derived2: public Base {
    int newVal;
    public:
    Derived2() {}
};


int main() {

    Base* b;
    Derived1* d1;
    Derived2* d2;

    // basics of static cast - we are casting same class - i.e derived1 will shadow derived1 (base is casted to derived1)
    b = new Derived1;
    // Derived1* d1 = b; //  value of type "Base *" cannot be used to initialize an entity of type "Derived1 *"
    d1 = static_cast<Derived1*>(b);
    d1->print();

    // Issue with static cast - we are casting its sibling class. ie. derived 1 will shadown derived 2 after casting.
    b = new Derived2;
    d1 = static_cast<Derived1*>(b);
    d1->print(); // this created undefined behaviour d1 is capable to access memory - mem leak

    // Dynamic cast will find this in runtime if casting is inappropriate.
    // b = new Derived2;
    
    try
    {   
        // d1 = dynamic_cast<Derived2*>(b); // compiler itself will throw error
        // d1 , in case of dynamic cast if casted inappropriately then d1 will be nullptr.
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    b = new Derived1;
    d1 = dynamic_cast<Derived1*>(b); // this will work only if base class has v table i.e some virtual method should be there in base class , f() abv
    d1->print();
    
    delete b;
    return 0;
}