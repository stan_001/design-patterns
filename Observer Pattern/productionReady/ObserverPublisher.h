#include <iostream>
#include <algorithm>
#include <mutex>
#include <vector>

using namespace std;

/**
 * @brief class ObserverNotifier is inherited by class who needs this pattern.
 * class A: public class ObserverNotifier<Interface which other class extend, so inturn on calling notifiyall in class A will 
 * trigger callbacks in classes which extends this interface>
 * classes which wants to be notified has to extend this interface.
 * @tparam Observer 
 */

template <typename Observer>
class ObserverNotifier {
    public:
        void addObserver(const shared_ptr<Observer>& observer) {
            if (observer == nullptr) {
                return;
            }

            lock_guard<mutex> lg(observerMutex);
            for (const auto& item: observers) {
                if (item.lock() == observer) {
                    return;
                }
            }
            observers.emplace_back(observer);
        }
        
        
        void removeObserver(const shared_ptr<Observer>& observer) {
            lock_guard<mutex> lg(observerMutex);
            observers.erase(
                remove_if(
                    observers.begin(),
                    observers.end(),
                    [&observer](const auto& item) {
                        auto sharedObserver = item.lock();
                        return sharedObserver == nullptr || sharedObserver == observer;
                    }
                ),
                observers.end()
            );
        }
              
        void cleanObservers() {
            lock_guard<mutex> lg(observerMutex);
            observers.clear();
        }

        virtual ~ObserverNotifier() = default;

    protected:
    template <
            typename Ret,
            typename Class,
            typename... Args,
            typename = enable_if_t<is_same<Observer, Class>::value>
        >
        void notifyAll(Ret Class::*func, Args&&... args) {
            std::unique_lock<mutex> ul(observerMutex);
            if (observers.empty()) return;
            // @note: This is very important and super interesting
            // we add all the non null observers into new list. 
            // It is added to new list becase we are creating snapshot of non-null observers
            // Client can abuse observer pattern and take it to next level by
            // adding or removing new observer on notification
            // ie. Subject ---> notify ---> observer1 ---> addObserver(observer2).
            // Vector will throw seg fault / memory leak, when adding elements during iteration
            // other way is to add the new observer (eg: observer2) into queue and notify them later. (in second pass) -- make this if design requires only.
            // While we craete snapshot we are removing the observers which are null.
            // finally notify every observers
            std::vector<std::shared_ptr<Observer>> observersToNotify;
             observers.erase(
                std::remove_if(
                        observers.begin(),
                        observers.end(),
                        [&observersToNotify](const auto& item) {
                            auto observer = item.lock();
                            if (observer == nullptr) {
                                return true;
                            }

                            observersToNotify.emplace_back(std::move(observer));
                            return false;
                        }),
                observers.end());

            ul.unlock();
            
            for (auto& item: observersToNotify) {
                // same as bind(func, item.get(), forward<Args>(args)...)()
                (*item.*func)(forward<Args>(args)...);
            }
        }

    private:
        mutex observerMutex;
        vector<weak_ptr<Observer>> observers;
};


class IObserver {
    public:
    virtual void update() = 0;
};

class ObserverClass1: public IObserver {
    void update() {}
};

class ObserverClass2: public IObserver {
    void update() {}
};

class ObserverClass3: public IObserver {
    void update() {}
};

// This inturn make subjectclass as observernotifier
class SubjectClass : public ObserverNotifier<IObserver> {
    
    template<typename Func, typename... Args>
    inline void notifyObservers(Func&& func, Args&& ...args) {
        notifyAll(func, std::forward<Args>(args)...);
    }

    // class contains a method to be called to notify all other functions
    void onEvent() {
        // provide arguments if necessary
        notifyObservers(&IObserver::update);
    }
};