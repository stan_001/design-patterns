#include <iostream>
#include <vector>
#include <mutex>
#include <memory>
#include <algorithm>

using namespace std;
//  g++ -std=c++17 .\ObserverNotifierRealTimeInCPP.cpp -o ObserverNotifier.exe
enum class ProximityValues {
    SHORT,
    MID,
    FAR
};

// helper
ostream& operator<<(ostream& ostr, ProximityValues pv) {
    const string proxName[] = {"SHORT", "MEDIUM", "FAR"};
    return ostr << proxName[(int)pv];
}

class IProximityObservers {
    public:
    virtual void update(ProximityValues) = 0;
};

class PhoneApplication: public IProximityObservers {
    public:
     void update(ProximityValues pv) {
        cout << "PhoneApplication Proximity value received " << (pv) << endl; 
    }
};

class ScreenBrightnessController:  public IProximityObservers {
    public:
    void update(ProximityValues pv) {
        cout << "ScreenBrightnessController Proximity value received " << (pv) << endl; 
    }
};

class ProximitySensor {
    public:
        ProximitySensor() {
        }

        // sensor hardware gives me proximity value
        void notify() {
            // this is value we get from sensors by some means
            ProximityValues pv = ProximityValues::FAR; // at present this is hard coded.
            lock_guard<mutex> lg(m);

            cout << "Notifiying Observers"<< endl;

            for(auto& item: m_proximityObservers) {
                auto observer = item.lock();
                observer.get()->update(pv);
            }
        }

        bool subscribe(const shared_ptr<IProximityObservers>& proximityOberverProvided) {
            
            if (nullptr == proximityOberverProvided) {
                cout << "Provided nullptr for subscription" << endl;
                return false;
            }

            lock_guard<mutex> lg(m);

            // check for its presence , we should not use expired() to check 
            // presence of weak pointer becasue it is not thread safe.
            for (const auto& item: m_proximityObservers) {
                if (item.lock() == proximityOberverProvided) {
                    cout << "Already subscribed" << endl;
                    return true;
                }
            }
            cout << "Added subscriber" << endl;
            m_proximityObservers.emplace_back(proximityOberverProvided);
            return true;            
        }

        bool unSubscribe(const shared_ptr<IProximityObservers>& proximityOberverProvided) {
             if (nullptr == proximityOberverProvided) {
                cout << "Provided nullptr for unsubscription" << endl;
                return false;
            }

            lock_guard<mutex> lg(m);
            /**
             * @brief ERASE & REMOVE_IF IDIOM - std::remove_if swaps elements inside the vector in order to put all elements that do not match the predicate towards the beginning of the container. This means that if the predicate (body of the lambda function) returns true, then that element will be placed at the end of the vector.

             * remove_if then **returns an iterator which points to the first element which matches the predicate **. In other words, an iterator to the first element to be removed.

             * std::vector::erase erases the range starting from the returned iterator to the end of the vector, such that all elements that match the predicate are removed. 
             */
            m_proximityObservers.erase(
                remove_if(m_proximityObservers.begin(), m_proximityObservers.end(),
                    [&proximityOberverProvided](const auto& item) {
                        auto observerInList = item.lock();
                        return (observerInList == proximityOberverProvided);
                    }
                ), m_proximityObservers.end()
            );

            return true;
        }

    private:
        mutex m;
        std::vector<weak_ptr<IProximityObservers>> m_proximityObservers;

};


int main() {
    auto ps = make_shared<ProximitySensor>();
    auto pa = make_shared<PhoneApplication>();
    auto sbc = make_shared<ScreenBrightnessController>();

    ps->subscribe(pa);
    ps->subscribe(sbc);

    ps->notify();

    ps->subscribe(sbc);

    ps->unSubscribe(sbc);
    ps->notify();
    
    return 0;
}