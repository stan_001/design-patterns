#include <iostream>

using namespace std;

// g++ -std=c++17 file.cpp -o executable
// intent - proimity sensor communicate to phoneApplication and ScreenBrghtnessApp


enum class ProximityValues {
    SHORT,
    MID,
    FAR
};

// helper
ostream& operator<<(ostream& ostr, ProximityValues pv) {
    const string proxName[] = {"SHORT", "MEDIUM", "FAR"};
    return ostr << proxName[(int)pv];
}

class PhoneApplication {
    public:
     void update(ProximityValues pv) {
        cout << "PhoneApplication Proximity value received " << (pv) << endl; 
    }
    
};

class ScreenBrightnessController {
    public:
    void update(ProximityValues pv) {
        cout << "ScreenBrightnessController Proximity value received " << (pv) << endl; 
    }
};


class ProximitySensor {
    public:
        ProximitySensor(PhoneApplication* pa, ScreenBrightnessController* sbc): m_pa(pa), m_sbc(sbc) {
        }
        // sensor hardware gives me proximity value
        void notify() {
            ProximityValues pv = ProximityValues::FAR;
            m_pa->update(pv);
            m_sbc->update(pv);
        }
    
    private:
        PhoneApplication* m_pa;
        ScreenBrightnessController* m_sbc;

};


int main() {
    auto ps = new ProximitySensor(new PhoneApplication(), new ScreenBrightnessController());
    ps->notify();
    return 0;
}