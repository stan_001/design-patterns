#include <iostream>
#include <string>
#include <memory>

/**
 * @brief Porblems with Inheritance
 * 1. Sibiling inheritance causes code duplication. 
 *                  Node1
 *                 /     \ 
 *             Node2 ---- Node3 (we cannot share code between 2 siblings - so code duplication is needed)
 *              /  \      /  \ 
 *         Node4  Node5 Node6 Node7
 * 
 * 2. Provision to add new feature or update new feature is very teadious task that we need to update in many files,
 * because of code redundancy.
 * 
 * 3. When ever we need to update the behavior of any class , say  tomorrow rubber duck will fly with hyperloop :p then
 * we need to open the class and update the method - this violates open closed principle we should not open to edit existing 
 * things in the class but we can add new things in same class. (completely new)
 * 
 * Note: collect2: error: ld returned 1 exit status means the virtual functions are not implemented so thre is compile 
 * time error during linking phase
 */

class Duck {
    protected:
    std::string name;
    public:
    Duck(const std::string& _name): name(_name) {}
    virtual void fly() = 0;
    virtual void quack() = 0;
};

class WildDuck: public Duck {
    public:
    WildDuck(const std::string& _name): Duck(_name) {
    }
    void fly() override {
        std::cout << name.c_str() << " Wild duck Im flying with feathers" << std::endl;
    }
    void quack() override {
        std::cout << name.c_str()  << " Wild duck quack quack" << std::endl;
    }
};

class RubberDuck: public Duck {
    public:
    RubberDuck(const std::string& _name): Duck(_name) {
    }
    void fly() override {
        std::cout <<  name.c_str() << " Rubber Duck Im flying with rocket" << std::endl;
    }
    void quack() override {
        std::cout <<  name.c_str() << " Rubber Duck squeak squeak" << std::endl;
    }
};

// Below is the way consumers of our code will use our code
int main()
{
  Duck* wildDuck1 = new WildDuck("Blacky");
  wildDuck1->fly();
  wildDuck1->quack();
  std::cout << "---------------" << std::endl;
  auto rubberDuck = std::make_shared<RubberDuck>("Yellowy");
  rubberDuck->quack();
  rubberDuck->fly();
}

/**
 * @brief OUT
 * PS > .\IssuesWithInheritance.exe
 * Blacky Wild duck Im flying with feathers 
 * Blacky Wild duck quack quack
 * ---------------
 * Yellowy Rubber Duck squeak squeak        
 * Yellowy Rubber Duck Im flying with rocket
 */