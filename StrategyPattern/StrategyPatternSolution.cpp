#include <memory>
#include <iostream>
#include <string>

using namespace std;

class IFlyingBehavior { // @note : action is export as interface. How to fly is developer dependent. Developer has provision
    // to chose how to fly, dev know what all fly behaviors exists and simply assign that to object(well something similar to builder patter)
    // this is similar to command pattern where in  command pattern developers uses this method to invoke the 
    // command i.e execute() will be called by other developers but here we ouselves consume this interface mostly
    // we use execute()~ here it is termed as fly() and used in Duck class and in that class fly() method will be 
    // wrapped with some other functions like performfly() this performfly is used by other developers.
    public:
        virtual void fly() = 0;
};

class FlyWithFeathers: public IFlyingBehavior {
    void fly() {
        cout << "Fying with feathers" << endl;
    }
};

class FlyingWithRocket: public IFlyingBehavior {
    void fly() {
        cout << "Fying with Rocket Boosters" << endl;
    }
};

class IQuackBehavior {
    public:
        virtual void quack() = 0;
};

class Quack: public IQuackBehavior {
    public:
    void quack() {
        cout << "Quack Quack" << endl;
    }
};

class Squeak: public IQuackBehavior {
    public:
    void quack() {
        cout << "Squeak Sqeuak" << endl;
    }
};


class Duck {
    protected:
    std::string name;
    private:
    shared_ptr<IFlyingBehavior> flyingBehavior;
    shared_ptr<IQuackBehavior> quackingBehavior;

    public:
    Duck(const std::string& _name): name(_name) {}

    void performFly() {
        flyingBehavior->fly();
    }

    void performQuack() {
        quackingBehavior->quack();
    }

    bool setFlyingBehavior(const shared_ptr<IFlyingBehavior>& fb) {
        flyingBehavior = move(fb);
        return true;
    }

    bool setQuackBehavior(const shared_ptr<IQuackBehavior>& qb) {
        quackingBehavior = move(qb);
        return true;
    }

};

class WildDuck: public Duck {
    public:
    WildDuck(const std::string& _name): Duck(_name) {}

    void Display() {
        cout << "Iam Wild Duck" << endl;
    }
};

class RubberDuck: public Duck {
    public:
    RubberDuck(const std::string& _name): Duck(_name) {}
    void Display() {
        cout << "Iam Rubber Duck" << endl;
    }
};


int main() {
    // creating behaviors
    shared_ptr<IFlyingBehavior> flywithFeather = make_shared<FlyWithFeathers>();
    shared_ptr<IQuackBehavior> justQuack = make_shared<Quack>();
    // creating duck and setting behavior - this can be altered later too based on business req
    shared_ptr<Duck> blacky = make_shared<WildDuck>("Blacky");
    blacky->setFlyingBehavior(flywithFeather);
    blacky->setQuackBehavior(justQuack);
    // developers finally calls the method.
    blacky->performFly();
    blacky->performQuack();
}

/**
 * @brief OUT
 * PS> .\StrategyPatternSolution.exe
 * Fying with feathers
 * Quack Quack
 * 
 */