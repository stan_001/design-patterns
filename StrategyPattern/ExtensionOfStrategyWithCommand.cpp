#include <memory>
#include <iostream>
#include <string>
#include <queue>
#include <functional>

using namespace std;

class IFlyingBehavior {
    public:
        virtual void fly() = 0;
};

class FlyWithFeathers: public IFlyingBehavior {
    void fly() {
        cout << "Flying with feathers" << endl;
    }
};

class FlyingWithRocket: public IFlyingBehavior {
    void fly() {
        cout << "Flying with Rocket Boosters" << endl;
    }
};

class IQuackBehavior {
    public:
        virtual void quack() = 0;
};

class Quack: public IQuackBehavior {
    public:
    void quack() {
        cout << "Quack Quack" << endl;
    }
};

class Squeak: public IQuackBehavior {
    public:
    void quack() {
        cout << "Squeak Sqeuak" << endl;
    }
};


class Duck {
    protected:
    std::string name;
    private:
    shared_ptr<IFlyingBehavior> flyingBehavior;
    shared_ptr<IQuackBehavior> quackingBehavior;

    public:
    Duck(const std::string& _name): name(_name) {}

    void performFly() {
        flyingBehavior->fly();
    }

    void performQuack() {
        quackingBehavior->quack();
    }

    bool setFlyingBehavior(const shared_ptr<IFlyingBehavior>& fb) {
        flyingBehavior = move(fb);
        return true;
    }

    bool setQuackBehavior(const shared_ptr<IQuackBehavior>& qb) {
        quackingBehavior = move(qb);
        return true;
    }

};

class WildDuck: public Duck {
    public:
    WildDuck(const std::string& _name): Duck(_name) {}

    void Display() {
        cout << "Iam Wild Duck" << endl;
    }
};

class RubberDuck: public Duck {
    public:
    RubberDuck(const std::string& _name): Duck(_name) {}
    void Display() {
        cout << "Iam Rubber Duck" << endl;
    }
};

/**
 * @brief Intent of command pattern
 * Command pattern is similar to strategy consider the text editor like quip (collaborative edit) where the 
 * algorithms are need to adapt text entered by different people across the world so algorithms could be language used
 * font styling used, LTR or RTL etc. Those can be handled as strategy rather having everything in receiver class. 
 * Our Duck Class is Receiver and we need to provide provision to queue the commands that developers make and execute them
 * like online game flappy birds and there are many users with duck they trigger fly command to fly 2 cm up for single command.
 * we need to queue all users command and execute. 
 */

// Implementation of execution Queue - this can be used my other service to just pick up command
// execute the commands in Queue. (TOday queue implementation will not work - check how to use unique pointer with stl queues.)
template <typename T>
class Queue {
    private:
        std::queue<T&> q;
    public:
        void enqueueCommand(const T& commandReference) {
            q.push(commandReference);
        }
        int getSizeOfQueue() {
            return q.size(); 
        }
        T& dequeueCommand() {
            return q.pop();
        }
};

class ICommand {
    public:
    virtual void execute() = 0;
};

class FlyCommand: public ICommand {
    private:
        function<void()> flyToBeCalled;
    public:
    FlyCommand(function<void()> methodInReceiverToCall):
        flyToBeCalled(methodInReceiverToCall)
    {}
    void execute() {
        flyToBeCalled();
    }
    
};

int main() {
    // creating behaviors
    shared_ptr<IFlyingBehavior> flywithFeather = make_shared<FlyWithFeathers>();
    shared_ptr<IQuackBehavior> justQuack = make_shared<Quack>();
    // creating duck and setting behavior - this can be altered later too based on business req
    shared_ptr<Duck> blacky = make_shared<WildDuck>("Blacky");
    blacky->setFlyingBehavior(flywithFeather);
    blacky->setQuackBehavior(justQuack);
    // developers finally calls the method.
    blacky->performFly();
    blacky->performQuack();

    cout << "----------------" << endl;
    auto testCommand = make_unique<FlyCommand>(bind(&Duck::performFly, blacky));
    testCommand->execute();

}

/**
 * @brief OUT
 * 
 */