#pragma once
#include <iostream>
#include "DosaSpecification.h"

using namespace std;

class Dosa
{
public:
    Dosa(string name, int id, int cost, DosaSpecification* spec)
    {
        m_id = id;
        m_cost = cost;
        m_name = name;
        m_spec = spec;
    }

    string getName();

    int getCost(DosaSpecification* dosaSpec);

    int getId() const;

    DosaSpecification* getSpec();

private:
    int m_id;
    int m_cost;
    string m_name;
    DosaSpecification* m_spec;
};