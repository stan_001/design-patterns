#include "Dosa.h"

string Dosa::getName()
{
    return m_name;
}

int Dosa::getId() const
{
    return m_id;
}

DosaSpecification *Dosa::getSpec()
{
    return m_spec;
}

int Dosa::getCost(DosaSpecification* dosaSpec)
{
    cout << "Base cost "<< m_cost << endl;
    if (dosaSpec->getNumberOfCondiments() == Condiments::FIVE)
    {
        m_cost = m_cost + 5;
    }

    if (dosaSpec->getNumberOfCondiments() == Condiments::SEVEN)
    {
        m_cost = m_cost + 10;
    }
    if (dosaSpec->hasAloo())
    {
        m_cost = m_cost + 3;
    }
    if (dosaSpec->hasOnion())
    {
        m_cost = m_cost + 5;
    }
    if (dosaSpec->hasCarrot())
    {
        m_cost = m_cost + 7;
    }
    if (dosaSpec->hasChanna())
    {
        m_cost = m_cost + 9;
    }

    // problematic functions
    if (dosaSpec->hasDoubleAloo())
    {
        m_cost = m_cost + 6;
    }

    if (dosaSpec->hasDoubleCarrot())
    {
        m_cost = m_cost + 14;
    }

    cout << "Final cost "<< m_cost << endl;
    return m_cost;
}
