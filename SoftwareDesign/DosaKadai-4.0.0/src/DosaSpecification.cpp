#include "DosaSpecification.h"

DosaSize DosaSpecification::getSize()
{
    return m_size;
}

Condiments DosaSpecification::getNumberOfCondiments()
{
    return m_numberOfCondiments;
}

bool DosaSpecification::matches(DosaSpecification *searchSpec)
{
    auto requiredDosaSize = searchSpec->getSize();

    if (searchSpec->getNumberOfCondiments() != this->getNumberOfCondiments())
    {
        return false;
    }

    if ((requiredDosaSize != DosaSize::ANY) && requiredDosaSize != this->getSize())
    {
        return false;
    }
    return true;
}

bool DosaSpecification::hasAloo()
{
    return m_aloo;
}

bool DosaSpecification::hasChanna()
{
    return m_channa;
}

bool DosaSpecification::hasOnion()
{
    return m_onion;
}

bool DosaSpecification::hasCarrot()
{
    return m_carrot;
}

void DosaSpecification::setAloo()
{
    m_aloo = true;
}

void DosaSpecification::setChanna()
{
    m_channa = true;
}

void DosaSpecification::setOnion()
{
    m_onion = true;
}

void DosaSpecification::setCarrot()
{
    m_carrot = true;
}

// problematic functions
void DosaSpecification::setDoubleAloo()
{
    m_doubleAloo = true;
}

void DosaSpecification::setDoubleCarrot()
{
    m_doubleCarrot = true;
}

bool DosaSpecification::hasDoubleAloo()
{
    return m_doubleAloo;
}

bool DosaSpecification::hasDoubleCarrot()
{
    return m_doubleCarrot;
}
