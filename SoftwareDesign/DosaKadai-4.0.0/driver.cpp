#include <iostream>
#include <memory>
#include "include/DosaSpecification.h"
#include "include/MenuItems.h"
#include "include/Dosa.h"

using namespace std;

shared_ptr<MenuItems> init()
{
    // shop owner add dosa into menu items
    shared_ptr<MenuItems> menu = make_shared<MenuItems>();
    menu->addItem(Dosa("Masala Dosa", 1, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Masala Dosa", 2, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Masala Dosa", 3, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Onion Dosa", 4, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Onion Dosa", 5, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Onion Dosa", 6, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 7, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Channa Dosa", 8, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Channa Dosa", 9, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 10, 5, new DosaSpecification(DosaSize::SMALL, Condiments::FIVE)));
    menu->addItem(Dosa("Channa Dosa", 11, 20, new DosaSpecification(DosaSize::LARGE, Condiments::FIVE)));
    return menu;
}

int main()
{
    auto menuItems = init();
    auto dosaSpec = new DosaSpecification(DosaSize::SMALL, Condiments::FIVE);

    // new toppings added on top of regular dosa with 5 condiments
    dosaSpec->setAloo();
    dosaSpec->setDoubleCarrot();

    auto searchResult = menuItems->search(dosaSpec);

    if (searchResult.empty())
    {
        cout << " No Items found " << endl;
    }
    
    for (auto item : searchResult)
    {
        if (item != nullptr)
        {
            cout << "---------->here is your dosa!<------- " << endl
                << "id: " << item->getId() << endl
                 << "name: " << item->getName() << endl
                 << "cost: " << item->getCost(dosaSpec) << endl;
        }
    }

    delete dosaSpec;
    return 0;
}
