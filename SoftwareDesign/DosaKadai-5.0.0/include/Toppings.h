#include <iostream>

using namespace std;

class ITiffinItems {
    public:
    virtual int getCost() = 0;
    virtual ~ITiffinItems() {}
};

class AddOnDecorator: public ITiffinItems {
    public:
        virtual void description() = 0;

    AddOnDecorator() = default;
    AddOnDecorator(ITiffinItems* tiffinItem) : m_tiffinItem(tiffinItem) {}

    protected:
        ITiffinItems* m_tiffinItem;
};

class AlooTopping: public AddOnDecorator {
    public:

        AlooTopping(ITiffinItems* tiffinItem): AddOnDecorator(tiffinItem) {}

        void description() {
            cout << "Aloo topping" << endl;
        }

        int getCost() {
            return m_tiffinItem->getCost() + 5;
        }
};

class ChannaTopping: public AddOnDecorator {
    public:

        ChannaTopping(ITiffinItems* tiffinItem): AddOnDecorator(tiffinItem) {}

        void description() {
            cout << "Channa topping" << endl;
        }

        int getCost() {
            return m_tiffinItem->getCost() + 3;
        }
};

class OnionTopping: public AddOnDecorator {
    public:

        OnionTopping(ITiffinItems* tiffinItem): AddOnDecorator(tiffinItem) {}
        void description() {
            cout << "Onion topping" << endl;
        }

        int getCost() {
            return m_tiffinItem->getCost() + 7;
        }
};

class CarrotTopping: public AddOnDecorator {
    public:

        CarrotTopping(ITiffinItems* tiffinItem): AddOnDecorator(tiffinItem) {}

        void description() {
            cout << "Carrot topping" << endl;
        }

        int getCost() {
            return m_tiffinItem->getCost() + 9;
        }
};