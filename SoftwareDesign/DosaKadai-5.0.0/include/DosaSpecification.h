#pragma once
enum class DosaSize
{
    ANY = 1,
    REGULAR,
    SMALL,
    LARGE
};

enum class Condiments
{
    BASIC = 1,
    FIVE,
    SEVEN
};

class DosaSpecification
{
public:
    DosaSpecification(DosaSize size, Condiments numberOfCondiments = Condiments::BASIC)
    {
        m_size = size;
        m_numberOfCondiments = numberOfCondiments;
    }

    DosaSize getSize();

    Condiments getNumberOfCondiments();

    bool matches(DosaSpecification *searchSpec);

private:
    Condiments m_numberOfCondiments;
    DosaSize m_size;
    bool m_aloo = false, m_carrot = false, m_channa = false, m_onion = false, m_doubleAloo = false, m_doubleCarrot = false;
};