# DOSA KADAI version 2.0.0
In this project, we will be creating a cpp project which vends dosa tokens to customers. By creating this project, we will learn together 
how to write code which satisifies customer requirements and accomodate new / changing requirements by customer.

## Customer Requirement
Design software which can be used by customers to,
- R1: Search dosa from menuitems, by entering ingredients + cost that customer can afford.
- R2: Owner should be able to add new dosa types to menuitems
- R3: given id of dosa customer need to get dosa from menuitems. This is useful for customers who are unaware of dosa types (travellers, tourists etc).

## Issues with Design
- Unable to provide dosa with customized topping - causing class explosion problem
- combining different topping and serving for customer is super difficult.

### FIXED
- Forcing user to enter right values to search dosa. This causes lose of customers. FIXED (customer use case is fulfilled)
- To add new properties we just need to modify only one class which is DosaSpecification using Encapsulation.
- Added sophisticated way to update the cost for items and toppings added

### UNFIXED ISSUES
NIL

## What is good software should have in common ? 
Good software should follow open closed principle. 
[Open Closed Principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle) - Classes, modules, functions should be open for extension but closed for modification.

## IMPORTANT STEPS OF SOFTWARE DEVELOPMENT
- Software should do what customer wants to do ! - 2.0.0 version solve customer use case.
- Apply Object Oriented Principles to add flexibility - 3.0.0 Used encapsulation so in future anything need to be added only DosaSpecification class can be extended.
- Strive for reusable design with design patterns. - 5.0.0 used decorator pattern to solve this issue of dynamic cost updates. loosley coupled architecture

### Wanted to extend this ? 
How can we incorporate discount for payment mode being used ? debit card / credit card / upi ? 


