#include <iostream>
#include <memory>
#include "include/DosaSpecification.h"
#include "include/MenuItems.h"
#include "include/Dosa.h"

using namespace std;

shared_ptr<MenuItems> init()
{
    // shop owner add dosa into menu items
    shared_ptr<MenuItems> menu = make_shared<MenuItems>();
    menu->addItem(Dosa("Masala Dosa", 1, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Masala Dosa", 2, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Masala Dosa", 3, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Onion Dosa", 4, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Onion Dosa", 5, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Onion Dosa", 6, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 7, 10, new DosaSpecification(DosaSize::REGULAR)));
    menu->addItem(Dosa("Channa Dosa", 8, 5, new DosaSpecification(DosaSize::SMALL)));
    menu->addItem(Dosa("Channa Dosa", 9, 20, new DosaSpecification(DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 10, 5, new DosaSpecification(DosaSize::SMALL, Condiments::FIVE)));
    menu->addItem(Dosa("Channa Dosa", 11, 20, new DosaSpecification(DosaSize::LARGE, Condiments::FIVE)));
    return menu;
}

int main()
{
    auto menuItems = init();
    auto dosaSpec = new DosaSpecification(DosaSize::SMALL, Condiments::FIVE);

    // basic item customer is looking for
    auto searchResult = menuItems->search(dosaSpec);

    if (searchResult.empty())
    {
        cout << " No Items found " << endl;
    }

    for (auto item : searchResult)
    {
        cout << "---------->here is your dosa<------- " << endl
             << "id: " << item->getId() << endl
             << "name: " << item->getName() << endl
             << "Base cost: " << item->getCost() << endl;
    }

    int dosaSelectedByCustomer;
    cout << "Enter id from search results : " ;
    cin >> dosaSelectedByCustomer;

    // handle errors if id provided is wrong
    // assuming customer will give right id always

    // adding toppings as per customer wish

    ITiffinItems* customerPrefferedDosa = searchResult.at(0);
    
    customerPrefferedDosa = new AlooTopping(customerPrefferedDosa);
    customerPrefferedDosa = new CarrotTopping(customerPrefferedDosa);
    customerPrefferedDosa = new OnionTopping(customerPrefferedDosa);

    cout << "New cost" << customerPrefferedDosa->getCost();

    delete dosaSpec;
    delete customerPrefferedDosa;
    return 0;
}
