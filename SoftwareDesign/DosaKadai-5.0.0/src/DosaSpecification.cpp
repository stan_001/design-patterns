#include "DosaSpecification.h"

DosaSize DosaSpecification::getSize()
{
    return m_size;
}

Condiments DosaSpecification::getNumberOfCondiments()
{
    return m_numberOfCondiments;
}

bool DosaSpecification::matches(DosaSpecification *searchSpec)
{
    auto requiredDosaSize = searchSpec->getSize();

    if (searchSpec->getNumberOfCondiments() != this->getNumberOfCondiments())
    {
        return false;
    }

    if ((requiredDosaSize != DosaSize::ANY) && requiredDosaSize != this->getSize())
    {
        return false;
    }
    return true;
}