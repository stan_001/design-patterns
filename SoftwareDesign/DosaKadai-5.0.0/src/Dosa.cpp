#include "Dosa.h"

string Dosa::getName()
{
    return m_name;
}

int Dosa::getId() const
{
    return m_id;
}

DosaSpecification *Dosa::getSpec()
{
    return m_spec;
}

int Dosa::getCost()
{
    return m_cost;
}
