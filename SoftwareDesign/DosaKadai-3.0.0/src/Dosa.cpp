#include "Dosa.h"

string Dosa::getName()
{
    return m_name;
}

int Dosa::getId() const
{
    return m_id;
}

DosaSpecification* Dosa::getSpec()
{
    return m_spec;
}

int Dosa::getCost()
{
    if (m_spec->getNumberOfCondiments() == Condiments::FIVE)
    {
        return m_cost + 5;
    }

    if (m_spec->getNumberOfCondiments() == Condiments::SEVEN)
    {
        return m_cost + 10;
    }
    return m_cost;
}
