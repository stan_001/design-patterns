#include "DosaSpecification.h"

Topping DosaSpecification::getTopping()
{
    return m_topping;
}

DosaSize DosaSpecification::getSize()
{
    return m_size;
}

Condiments DosaSpecification::getNumberOfCondiments()
{
    return m_numberOfCondiments;
}

bool DosaSpecification::matches(DosaSpecification *searchSpec)
{
    auto requiredDosaSize = searchSpec->getSize();
    auto requiredDosaTopping = searchSpec->getTopping();

    if (searchSpec->getNumberOfCondiments() != this->getNumberOfCondiments())
    {
        return false;
    }

    if ((requiredDosaTopping != Topping::ANY_TOPPING) && requiredDosaTopping != this->getTopping())
    {
        return false;
    }

    if ((requiredDosaSize != DosaSize::ANY) && requiredDosaSize != this->getSize())
    {
        return false;
    }
    return true;
}
