#include <iostream>
#include <memory>
#include "include/DosaSpecification.h"
#include "include/MenuItems.h"
#include "include/Dosa.h"

using namespace std;

shared_ptr<MenuItems> init()
{
    // shop owner add dosa into menu items
    shared_ptr<MenuItems> menu = make_shared<MenuItems>();
    menu->addItem(Dosa("Masala Dosa", 1, 10, new DosaSpecification(Topping::ALOO, DosaSize::REGULAR)));
    menu->addItem(Dosa("Masala Dosa", 2, 5, new DosaSpecification(Topping::ALOO, DosaSize::SMALL)));
    menu->addItem(Dosa("Masala Dosa", 3, 20, new DosaSpecification(Topping::ALOO, DosaSize::LARGE)));

    menu->addItem(Dosa("Onion Dosa", 5, 10, new DosaSpecification(Topping::ONION, DosaSize::REGULAR)));
    menu->addItem(Dosa("Onion Dosa", 6, 5, new DosaSpecification(Topping::ONION, DosaSize::SMALL)));
    menu->addItem(Dosa("Onion Dosa", 7, 20, new DosaSpecification(Topping::ONION, DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 8, 10, new DosaSpecification(Topping::CHANNA, DosaSize::REGULAR)));
    menu->addItem(Dosa("Channa Dosa", 9, 5, new DosaSpecification(Topping::CHANNA, DosaSize::SMALL)));
    menu->addItem(Dosa("Channa Dosa", 10, 20, new DosaSpecification(Topping::CHANNA, DosaSize::LARGE)));

    menu->addItem(Dosa("Channa Dosa", 11, 5, new DosaSpecification(Topping::ONION, DosaSize::SMALL, Condiments::FIVE)));
    menu->addItem(Dosa("Channa Dosa", 12, 20, new DosaSpecification(Topping::CHANNA, DosaSize::LARGE, Condiments::FIVE)));
    return menu;
}

int main()
{
    auto menuItems = init();
    auto dosaSpec = new DosaSpecification(Topping::ANY_TOPPING, DosaSize::ANY, Condiments::FIVE);
    auto searchResult = menuItems->search(dosaSpec);

    if (searchResult.empty())
    {
        cout << " No Items found " << endl;
    }
    
    for (auto item : searchResult)
    {
        if (item != nullptr)
        {
            cout << "---------->here is your dosa!<------- " << endl
                << "id: " << item->getId() << endl
                 << "name: " << item->getName() << endl
                 << "cost: " << item->getCost() << endl;
        }
    }

    delete dosaSpec;
    return 0;
}
