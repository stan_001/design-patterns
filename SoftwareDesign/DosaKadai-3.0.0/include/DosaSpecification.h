#pragma once
enum class DosaSize
{
    ANY = 1,
    REGULAR,
    SMALL,
    LARGE
};

enum class Topping
{
    ANY_TOPPING = 1,
    ALOO,
    CHANNA,
    CARROT,
    ONION
};

enum class Condiments
{
    BASIC = 1,
    FIVE,
    SEVEN
};

class DosaSpecification
{
public:
    DosaSpecification(Topping topping, DosaSize size, Condiments numberOfCondiments = Condiments::BASIC)
    {
        m_topping = topping;
        m_size = size;
        m_numberOfCondiments = numberOfCondiments;
    }

    Topping getTopping();

    DosaSize getSize();

    Condiments getNumberOfCondiments();

    bool matches(DosaSpecification *searchSpec);

private:
    Condiments m_numberOfCondiments;
    Topping m_topping;
    DosaSize m_size;
};