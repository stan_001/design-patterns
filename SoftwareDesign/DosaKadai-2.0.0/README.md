# DOSA KADAI version 2.0.0
In this project, we will be creating a cpp project which vends dosa tokens to customers. By creating this project, we will learn together 
how to write code which satisifies customer requirements and accomodate new / changing requirements by customer.

## Customer Requirement
Design software which can be used by customers to,
- R1: Search dosa from menuitems, by entering ingredients + cost that customer can afford.
- R2: Owner should be able to add new dosa types to menuitems
- R3: given id of dosa customer need to get dosa from menuitems. This is useful for customers who are unaware of dosa types (travellers, tourists etc).

## Issues with Design

### FIXED
- Forcing user to enter right values to search dosa. This causes lose of customers. FIXED (customer use case is fulfilled)

### UNFIXED ISSUES
- we need to touch almost all the classes if cost need to revised or new property need to be added for dosa, for example - if user needs condiments we need to touch Dosa class and MenuItems class. "If customer asked for more varity of chutney / sambar then shop can provide but we dont have way to charge for these extra items to customers"
- In large codebase making these kind of changes shall break the integrity of software. Modifying shall cause - Unit test breakage, Integration test breakage, Need to re-evaluate all test cases, changes in automation scripts etc.
- Need to manually update pricing everyday based on cost of ingredients used.

## What is good software should have in common ? 
Good software should follow open closed principle. 
[Open Closed Principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle) - Classes, modules, functions should be open for extension but closed for modification.

## IMPORTANT STEPS OF SOFTWARE DEVELOPMENT
- Software should do what customer wants to do ! - 2.0.0 version solve customer use case.
- Apply Object Oriented Principles to add flexibility
- Strive for reusable design with design patterns.


