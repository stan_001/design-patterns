#include "Dosa.h"

string Dosa::getName()
{
    return m_name;
}

int Dosa::getId() const
{
    return m_id;
}

Topping Dosa::getTopping()
{
    return m_topping;
}

DosaSize Dosa::getSize()
{
    return m_size;
}

Condiments Dosa::getNumberOfCondiments() {
    return m_numberOfCondiments;
}

int Dosa::getCost()
{
    if (m_numberOfCondiments == Condiments::FIVE) {
        return m_cost + 5;
    }

    if (m_numberOfCondiments == Condiments::SEVEN) {
        return m_cost + 10;
    }
    return m_cost;
}
