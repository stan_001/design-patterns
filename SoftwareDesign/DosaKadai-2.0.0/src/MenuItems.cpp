#include "MenuItems.h"

using namespace std;

vector<Dosa*> MenuItems::search(Topping toppings, DosaSize size, Condiments numberOfCondiments)
{   
    vector<Dosa*> searchResults;

    for (auto item = m_dosaItems.begin(); item != m_dosaItems.end(); item++)
    {   
        if (numberOfCondiments != item->getNumberOfCondiments()) {
            continue;
        }

        if ((toppings != Topping::ANY_TOPPING) && item->getTopping() != toppings)
        {
            continue;
        }
        if ((size != DosaSize::ANY) && size != item->getSize())
        {
            continue;
        }
        searchResults.push_back(new Dosa(*item));
    }

    return searchResults;
}

void MenuItems::addItem(Dosa dosa)
{
    m_dosaItems.push_back(dosa);
}

Dosa *MenuItems::getItem(int serialNumber)
{
    auto iterator = find_if(m_dosaItems.begin(), m_dosaItems.end(), [serialNumber](const Dosa &item)
                            { return item.getId() == serialNumber; });

    if (iterator != m_dosaItems.end())
    {
        return new Dosa(*iterator);
    }

    return nullptr;
}
