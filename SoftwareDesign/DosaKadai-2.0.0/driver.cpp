#include <iostream>
#include <memory>
#include "include/MenuItems.h"
#include "include/Dosa.h"

using namespace std;

shared_ptr<MenuItems> init()
{
    // shop owner add dosa into menu items
    shared_ptr<MenuItems> menu = make_shared<MenuItems>();
    menu->addItem(Dosa("Masala Dosa", 1, Topping::ALOO, DosaSize::REGULAR, 10));
    menu->addItem(Dosa("Masala Dosa", 2, Topping::ALOO, DosaSize::SMALL, 5));
    menu->addItem(Dosa("Masala Dosa", 3, Topping::ALOO, DosaSize::LARGE, 15));

    menu->addItem(Dosa("onion Dosa", 4, Topping::ONION, DosaSize::REGULAR, 10));
    menu->addItem(Dosa("onion Dosa", 5, Topping::ONION, DosaSize::SMALL, 5));
    menu->addItem(Dosa("onion Dosa", 6, Topping::ONION, DosaSize::LARGE, 15));

    menu->addItem(Dosa("carrot Dosa", 7, Topping::CARROT, DosaSize::REGULAR, 10));
    menu->addItem(Dosa("carrot Dosa", 8, Topping::CARROT, DosaSize::SMALL, 5));
    menu->addItem(Dosa("carrot Dosa", 9, Topping::CARROT, DosaSize::LARGE, 15));

    menu->addItem(Dosa("channa Dosa", 10, Topping::CHANNA, DosaSize::REGULAR, 10));
    menu->addItem(Dosa("channa Dosa", 11, Topping::CHANNA, DosaSize::SMALL, 5));
    menu->addItem(Dosa("channa Dosa", 12, Topping::CHANNA, DosaSize::LARGE, 15));

    menu->addItem(Dosa("Masala Dosa with 5 condiments", 13, Topping::ALOO, DosaSize::REGULAR, 10, Condiments::FIVE));
    menu->addItem(Dosa("Masala Dosa with 7 condiments", 14, Topping::ONION, DosaSize::REGULAR, 10, Condiments::SEVEN));

    return menu;
}

int main()
{
    auto menuItems = init();
    auto searchResult = menuItems->search(Topping::ANY_TOPPING, DosaSize::REGULAR, Condiments::FIVE);

    if (searchResult.empty())
    {
        cout << " No Items found " << endl;
    }
    
    for (auto item : searchResult)
    {
        if (item != nullptr)
        {
            cout << "---------->here is your dosa!<------- " << endl
                << "id: " << item->getId() << endl
                 << "name: " << item->getName() << endl
                 << "cost: " << item->getCost() << endl;
        }
    }
    return 0;
}
