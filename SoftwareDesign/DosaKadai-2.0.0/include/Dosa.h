#pragma once
#include <iostream>

using namespace std;


enum class DosaSize {
    ANY = 1,
    REGULAR,
    SMALL,
    LARGE
};

enum class Topping {
    ANY_TOPPING = 1,
    ALOO,
    CHANNA,
    CARROT,
    ONION
};

enum class Condiments {
    BASIC = 1,
    FIVE,
    SEVEN
};

class Dosa
{
public:
    Dosa(string name, int id, Topping topping, DosaSize size, int cost, Condiments numberOfCondiments =  Condiments::BASIC)
    {
        m_id = id;
        m_cost = cost;
        m_name = name;
        m_topping = topping;
        m_size = size;
        m_numberOfCondiments = numberOfCondiments;
    }

    string getName();

    Topping getTopping();

    DosaSize getSize();

    int getCost();

    int getId() const;

    Condiments getNumberOfCondiments();

private:
    int m_id;
    int m_cost;
    Condiments m_numberOfCondiments;
    string m_name;
    Topping m_topping;
    DosaSize m_size;
};