#pragma once
#include <iostream>
#include <algorithm>
#include <vector>

#include "Dosa.h"

using namespace std;

class MenuItems
{
public:
    vector<Dosa*> search(Topping topping, DosaSize size, Condiments numberOfCondiments);
    Dosa*getItem(int id);
    void addItem(Dosa dosa);

private:
    vector<Dosa> m_dosaItems;
};