#include "Dosa.h"

string Dosa::getName()
{
    return m_name;
}

int Dosa::getId() const
{
    return m_id;
}

string Dosa::getTopping()
{
    return m_topping;
}

string Dosa::getSize()
{
    return m_size;
}

int Dosa::getCost()
{   
    return m_cost;
}
