#pragma once
#include <iostream>

using namespace std;

class Dosa
{
public:
    Dosa(string name, int id, string topping, string size, int cost)
    {
        m_id = id;
        m_cost = cost;
        m_name = name;
        m_topping = topping;
        m_size = size;
    }

    string getName();

    string getTopping();

    string getSize();

    int getCost();

    int getId() const;

private:
    int m_id;
    int m_cost;
    string m_name;
    string m_topping;
    string m_size;
};