#pragma once
#include <iostream>
#include <algorithm>
#include <vector>

#include "Dosa.h"

using namespace std;

class MenuItems
{
public:
    vector<Dosa*> search(string topping, string size);
    Dosa*getItem(int id);
    void addItem(Dosa dosa);

private:
    vector<Dosa> m_dosaItems;
};