#include <iostream>
#include <memory>
#include "include/MenuItems.h"
#include "include/Dosa.h"

using namespace std;

shared_ptr<MenuItems> init()
{
    // shop owner add dosa into menu items
    shared_ptr<MenuItems> menu = make_shared<MenuItems>();
    menu->addItem(Dosa("Masala Dosa", 1, "aloo", "regular", 10));
    menu->addItem(Dosa("Masala Dosa", 2, "aloo", "small", 5));
    menu->addItem(Dosa("Masala Dosa", 3, "aloo", "large", 15));

    menu->addItem(Dosa("onion Dosa", 4, "onion", "regular", 10));
    menu->addItem(Dosa("onion Dosa", 5, "onion", "small", 5));
    menu->addItem(Dosa("onion Dosa", 6, "onion", "large", 15));

    menu->addItem(Dosa("carrot Dosa", 7, "carrot", "regular", 10));
    menu->addItem(Dosa("carrot Dosa", 8, "carrot", "small", 5));
    menu->addItem(Dosa("carrot Dosa", 9, "carrot", "large", 15));

    menu->addItem(Dosa("channa Dosa", 10, "channa", "regular", 10));
    menu->addItem(Dosa("channa Dosa", 11, "channa", "small", 5));
    menu->addItem(Dosa("channa Dosa", 12, "channa", "large", 15));

    return menu;
}

int main()
{
    auto menuItems = init();
    auto searchResult = menuItems->search("", "regular");

    if (searchResult.empty())
    {
        cout << " No Items found " << endl;
    }
    for (auto item : searchResult)
    {
        if (item != nullptr)
        {
            cout << "---------->here is your dosa!<------- " << endl
                 << "name: " << item->getName() << endl
                 << "cost: " << item->getCost() << endl
                 << "size: " << item->getSize() << endl;
        }
    }
    return 0;
}
