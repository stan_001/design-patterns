#include <iostream>

using namespace std;


class Base {
public:
    Base() {
        cout << "Base::Constructor" << endl;
    }
    virtual ~Base() { // virtual needed to call derived class destructor
        cout << "Base::Destructor" << endl;
    }
};

class Derived : public Base {

public:
    Derived() {
        cout << "Derived::Constructor" << endl;
    }
    ~Derived() { 
        cout << "Derived::Destructor" << endl;
    }
};
 
int main() {
    Derived* d = new Derived;
    Base* b = d;
    delete b;
}

// op: without virtual keyword in base.
// Base::Constructor
// Derived::Constructor
// Base::Destructor

// op: with virtual keyword in base.
// Base::Constructor
// Derived::Constructor
// Derived::Destructor
// Base::Destructor