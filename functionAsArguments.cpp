#include <iostream>
#include <functional>

using namespace std;
struct CustomStruct {
    int sub(int a, int b) { return a - b; }
};

void print() {
    cout << "This is print()" << endl;
}

int sum(int a, int b) {
    cout << "This is sum() method with argument and return type ";
    return a+b;
}

void callerMethod(void (*func)()) {
    func();
}

void callerMethod1(const function<void()>& func1) { // const is adder for rvalue conversion because we are using &
    func1();
}

void callerMethod2(const function<int(int,int)>& func3) {// const is adder for rvalue conversion because we are using &
    func3(45,123);
}

int custFunction(int a) {
    return a;
}

template <typename T>
T templateMethod(T arg) {
    cout << "This is templateMethod " << arg << endl;
}

int main()
{
    cout<<"Demo: Number of ways to pass function as argument" << endl;
    
    // basic function call using 3 ways
    
    // pointers to function 
    // create pointer function <return-type> <(*newFunctionPointerName)><() --> this will say that this of type function>
    void (*func)() = &print;
    print();
    // above passed into some other function 
    callerMethod(&print);
    
    // using std::function
    function<void()> func1 = &print;
    func1();
    // above passed into another function
    callerMethod1(&print);
    
    
    // Other famous way is passing lambda expressions
    callerMethod([](){
        cout << "This is printed from lambda" << endl;
    });
    callerMethod1([]() {
        cout << "This is printed from lambda1" << endl;
    });
    
    // funtion with arguments and return type 
    
    // pointers to functions
    int (*func2)(int, int) = &sum;
    cout << sum(12,45) << endl;
    
    function<int(int,int)> func3 = &sum;
    cout << func3(121,45) << endl;
    
    callerMethod2([=](int a, int b) -> int {
        cout << "this is lambda with argument and return type: " << a + b << endl;
        return 0;
    });
    
    // using bind to get address of function rather assigning using &functionName 
    // auto var = &fucntionName;
    auto var = std::bind(print); // here auto is function<returnType(arguments...)> var
    cout << "Type of var is " << typeid(var).name() << endl;
    function<void()> varr = std::bind(print);
    varr();
    var();
    // advantages of using bind is we can have arguments jumbled 
    auto var1 = std::bind(sum, std::placeholders::_2, std::placeholders::_1);
    cout << var1(456,90) << endl;
    
    // passing default argument in place of 2nd argument
    auto var2 = std::bind(sum, 1234, std::placeholders::_1);
    cout << var2(100) << endl;
    
    // bind can be nested too.
    auto var3 = std::bind(custFunction, std::placeholders::_1);
    auto var33 = std::bind(sum, var3, std::placeholders::_2);
    cout << var33(var3(90), 100) << endl;
    
    
    // template function
    
    templateMethod<int>(123);
    int (*tFunction)(int) = &templateMethod;
    tFunction(122);
    
    function<int(int)> ttFunction = &templateMethod<int>;
    ttFunction(121233);
    
    // note if the function is member function then class reference is needed when binding
    // Only using std::bind works because a non-static member function has an implicit first parameter of type (cv-qualified) YourType*, 
    // so in this case it does not match void(int). Hence the need for std::bind:
    // we need to pass the object reference where the binded function can be found.
    // example
    CustomStruct cs;
    auto memberFunc = std::bind(&CustomStruct::sub, &cs, 123, std::placeholders::_3);
    cout << memberFunc(54, 345, 12); // note we can pass any number of argument but bind helps in binding with necessary arguments. that is primary use of placeholders.
    // _1: 54, _2:345 , _3:12 here 12 is provided as second argumnet to method hence 123-13  =111 
    return 0;
}
