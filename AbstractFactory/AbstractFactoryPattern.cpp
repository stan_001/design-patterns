#include <iostream>
#include <memory>

using namespace std;

enum class InterfaceOSType
{
    WINDOWS,
    MAC,
    LINUX
};

// Lets have interface which helps to create button & text boxes
// these concrete classes with have behaviour in it & not the creation logic
class InterfaceButton
{
public:
    virtual void onPress() = 0;
    virtual void onHover() = 0;
};

class MacButton : public InterfaceButton
{
public:
    void onPress()
    {
        cout << "MacButton Press Triggered" << endl;
    }
    void onHover()
    {
        cout << "MacButton Hover Triggered" << endl;
    }
};

class WindownButton : public InterfaceButton
{
public:
    void onPress()
    {
        cout << "WindowButton Press Triggered" << endl;
    }
    void onHover()
    {
        cout << "WindowButton Hover Triggered" << endl;
    }
};

class LinuxButton : public InterfaceButton
{
public:
    void onPress()
    {
        cout << "LinuxButton Press Triggered" << endl;
    }
    void onHover()
    {
        cout << "LinuxButton Hover Triggered" << endl;
    }
};

class InterfaceTextBox
{
public:
    virtual void showText() = 0;
    virtual void onHover() = 0;
};

class MacTextBox : public InterfaceTextBox
{
public:
    void showText()
    {
        cout << "Mac TextBox Show Triggered" << endl;
    }
    void onHover()
    {
        cout << "Mac TextBox hover Triggered" << endl;
    }
};
class LinuxTextBox : public InterfaceTextBox
{
public:
    void showText()
    {
        cout << "Linux TextBox Show Triggered" << endl;
    }
    void onHover()
    {
        cout << "Linux TextBox hover Triggered" << endl;
    }
};
class WindowTextBox : public InterfaceTextBox
{
public:
    void showText()
    {
        cout << "Windows TextBox Show Triggered" << endl;
    }
    void onHover()
    {
        cout << "Windows TextBox hover Triggered" << endl;
    }
};

// Lets have factory class which helps in creating Button and TextBox object instances
class InterfaceFactory
{
public:
    virtual unique_ptr<InterfaceButton> createButton() = 0;
    virtual unique_ptr<InterfaceTextBox> createTextBox() = 0;
};

// OS specific class extend these Abstract factory to create buttons & Textbox
// abstract factory has signature of Interface return type - here it is IButton and Itextbox
// client uses abstract factory static method to create factory , -> creates buttons and textBox instance.
// client create InterfaceFactory using InterfaceAbstractFactory

class MacFactory : public InterfaceFactory
{
    std::unique_ptr<InterfaceButton> createButton()
    {
        cout << "Creating MAC Button" << endl;
        return make_unique<MacButton>();
    }

    std::unique_ptr<InterfaceTextBox> createTextBox()
    {
        cout << "Creating MAC TextBox" << endl;
        return make_unique<MacTextBox>();
    }
};

class WindowsFactory : public InterfaceFactory
{
    std::unique_ptr<InterfaceButton> createButton()
    {
        cout << "Creating Windows Button" << endl;
        return make_unique<WindownButton>();
    }

    std::unique_ptr<InterfaceTextBox> createTextBox()
    {
        cout << "Creating Windows TextBox" << endl;
        return make_unique<WindowTextBox>();
    }
};

class LinuxFactory : public InterfaceFactory
{
    std::unique_ptr<InterfaceButton> createButton()
    {
        cout << "Creating Linux Button" << endl;
        return make_unique<LinuxButton>();
    }

    std::unique_ptr<InterfaceTextBox> createTextBox()
    {
        cout << "Creating Linux TextBox" << endl;
        return make_unique<LinuxTextBox>();
    }
};

// lets create abstract factory, client uses this to create factory classes. Factory classes create concrete classes and concrete classes has behaviors in them.

class InterfaceAbstractFactory
{
public:
    static unique_ptr<InterfaceFactory> getInstance(InterfaceOSType osType)
    {
        if (osType == InterfaceOSType::WINDOWS)
        {
            return make_unique<WindowsFactory>();
        }
        
        if (osType == InterfaceOSType::LINUX) {
            return make_unique<LinuxFactory>();
        }

        cout << "Default creating MAC Factory instance" << endl;
        return make_unique<MacFactory>();
    }
};

int main()
{
    // client works on windows and needs windows button and textbox
    unique_ptr<InterfaceFactory> windowsFactory = InterfaceAbstractFactory::getInstance(InterfaceOSType::WINDOWS);

    auto windowsBtn1 = windowsFactory->createButton();
    windowsBtn1->onPress();

    auto windowsTextBox = windowsFactory->createTextBox();
    windowsTextBox->showText();

    return 0;
}