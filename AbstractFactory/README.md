# Abstract Factory
Abstract factory is extension of factory pattern. Factory design pattern helps to create objects from factories. Abs Factory creates factories which inturn create objects. (abs factory creates family of instance)

## Usage
* Used to create family of objects which inturn used to create object of specific type.
* Best exmaple of encapsulation

## Usecase
* Button and Textbox for operating systems
* Client need to provide what operating system they are working up on (factory pattern)
* based on this when client attempts to create a Button / TextBox class , they will be able to create OS specific ones.


