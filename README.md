# README #

Design Patterns RoadMap

### Roadmap details ###

* SOLID Principles
* Strategy Pattern
* Observer Pattern
* Decorator Pattern 
* Factory Pattern 
* Abstract Factory 
* Chain of responsibility
* Proxy Pattern 
* Null Object Pattern 
* State Pattern
* Composite Pattern 
* Adapter Pattern 
* Singleton Pattern 
* Builder Pattern
* Proptype Pattern 
* Bridge Pattern 
* Facade Pattern 
* Flyweight Pattern 
* Command Pattern 
* Interpreter Pattern 
* Iterator Pattern 
* Mediator Pattern 
* Memento Pattern 
* Visitor Pattern
* Template Method 

### Desing Problems ###
* Design Chess Game
* Design CricBuzz / Criket Info
* Design File System
* Design Splitwise
* Design ATM
* Design Trafin Light System
* Design Meeting Scheduler
* Design Online Voting System
* Design Lib Management System
* Design Online Hotel Booking System
* Design Car Booking Service like ola, uber
* Design True Caller
* Splitwise Simplify Algorithm / Optimal accounting balancing
* Design Vending Machine
* Design Notify-Me Button Functionality
* Design Coffee shop billing system
* Design Parking lot
* Design snake & ladder game
* Design Elevator System
* Design Car Rental System
* Design Logging System
* Design Tic-Tac-Toe game
* Design Book my show & Concurrency Handling
* Design Inventory management system
* Design Cache Mechanism
* Design LinkedIn
* Design Amazon
* Design Airline Management System
* Design Stock Exchange System
* Design Learning Management System
* Design a Calendar Application
* Design Payment System
* Design Chat based system
* Design food delivery app - zomato & swiggy


### Who do I talk to? 

* (Ravi) mailstoraviraaja@gmail.com
