#include <iostream>
#include <functional>

using namespace std;
// example1
void methodToBeInvoked() {
    cout << "methodToBeInvoked" << endl;
}

void exampleToInvokeBindMethod() {
    bind(methodToBeInvoked)();
}
// exmple2 
void methodWithArgToBeInvoked(const int& param1, const string& param2) {
    cout << "methodWithArgToBeInvoked " << param1 << " " << param2 << endl;
}

void exampleToInvokeBindWithArguements() {
    bind(methodWithArgToBeInvoked, 123, "asdas")();
}

// pick argument one by one and pass to function

void functionDealsWithOneArgAtaTime(const string& arg) {
    cout << "functionDealsWithOneArgAtaTime "<< arg << endl;
}

template <typename... Args>
void exampleToUnwrapArgumentExperiment(Args&& ...args) {
    (functionDealsWithOneArgAtaTime(args), ...);
    // forward<Args>(args)... -> only this will foward the rvalue referece but here it is like
    // (forward<Args>(args), ...) -> this is for unpacking each argument and send one by once
    (functionDealsWithOneArgAtaTime(forward<Args>(args)), ...);
}

// example of perfect forwarding i.e. forward all arguments at one go as rvalue.
class Foo {
    public:
    Foo(const int& i, const string& s, const vector<int>& v): // this constructor will create copy
            m_i(i),
            m_s(s),
            m_v(v) {
                cout << "Lvalue constructor invoked" << endl;
        }
    Foo(int&& i, string&& s, vector<int>&& v): // this will not create copy directly assign the values.
            m_i(i),
            m_s(s),
            m_v(move(v)) {
                cout << "Rvalue constructor invoked" << endl;
        }    
    private:
    int m_i;
    string m_s;
    vector<int> m_v;
};


class Bar {
    public:
    template <typename ...Args>
    void AddFoo(Args&& ...args) {
        // here forward args will forard all the arguments once at a time.
        v.emplace_back(forward<Args>(args)...);
    }
    private:
        vector<Foo> v;
};

// test method to check bind with forward args - send all argument at once.
void testForwardArgsCalle(const int& i, const string& s) { // imp that param should always match
    cout << " i: " << i << "s: " << s << endl;
}

template <typename ...Args>
void testForwardArgsCaller(Args&& ...args) {
    // simple arg forwarding
    testForwardArgsCalle(forward<Args>(args)...);

    // forward with bind
    bind(testForwardArgsCalle, forward<Args>(args)...)(); // invoke immediately after bind.
}

int main() {
    exampleToInvokeBindMethod();
    exampleToInvokeBindWithArguements();
    exampleToUnwrapArgumentExperiment("arg1", "arg2", "arg3");
    // exampleToUnwrapArgumentExperiment(1, 2, 3); this will not work because of type mismatch in functionDealsWithOneArgAtaTime()
    
    // basics of lvalue and rvalue construction
    vector<int> v = {1, 34, 12};
    int intVar = 10;
    Foo(intVar, "Ravi", v); // though "Ravi" string is rvalue others are lvalue so lvalue constructor is invoked.
    cout << " vector size when using lvalue - " << v.size() << endl;

    Foo(100, "somestring", move(v)); // rvalu constructor invoked. This can be leveraged when this class used in other class
    cout << " Vecotr is move so , temp is created and move , vector size when using rvalue - " << v.size() << endl;

    // use case of lvalue and rvalue construction - perfect forwarding
    // below will create foo and directly assign temp to the vector rather creating copy of Foo and destructing Foo after copying
    // ie. Foo(19, "asd", v) -> this will create Foo, then AddFoo will emplace the object inside vector by copying the object values
    // in vector space. Now Foo's destructor will be called to clean up.
    Bar b;
    b.AddFoo(Foo(19, "this is interesting", move(v))); // rvalue construct will be invoked

    // IMPORTANT 
    // forward arguments inside bind and call immediately
    // bind usually takes all params or gets placeholders to be substituted later down the line.
    
    testForwardArgsCaller(100, "fgs");
    
}
